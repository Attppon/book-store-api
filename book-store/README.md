
# Bookstore API

==========================
### 1. Run script create database

- script_book_store_db.sql

### 2. Set Database Configure
-  application.properties

### 3. Step to packaging & run.

- Build Package
```
	$ mvn clean package
```

- Run Maven
```
	$ mvn clean spring-boot:run
	
	