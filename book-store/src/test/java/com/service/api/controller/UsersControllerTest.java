package com.service.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.api.dao.UserSessionDao;
import com.service.api.domain.UsersSession;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.response.UserResponse;
import com.service.api.service.UserManagementService;
import com.service.api.util.StringUtils;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UsersControllerTest {

	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserManagementService userManagementService;
    
    @MockBean
    private UserSessionDao userSessionDao;
    
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    
    @Test
    public void getUsernSuccess() throws Exception {
    	
    	UserResponse response = new UserResponse();
    	response.setName("Mr.Attapon Janda");
    	
    	UsersSession usersSession = new UsersSession();
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(new Date());
		calendar.add(Calendar.SECOND, 6000);
		usersSession.setUssExpiryTime(calendar.getTime());
		
    	when(userSessionDao.find(any())).thenReturn(usersSession);
		when(userSessionDao.update(any(UsersSession.class))).thenReturn(1);
        when(userManagementService.getUser(any(BaseRequest.class))).thenReturn(response);
        
        mockMvc.perform(
                MockMvcRequestBuilders.get("/users")
                .header("sid", "74fe21e0-68ab-42d5-91b1-114e257e3e7a")
                .requestAttr("DATA_DETAILS", "{ }")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(new BaseRequest("74fe21e0-68ab-42d5-91b1-114e257e3e7a"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").isNotEmpty())
                .andReturn();
    }
    
}
