package com.service.api.dao;

import com.service.api.domain.Users;

public interface UsersDao {

	public Long insert(Users obj) throws Exception;
	
	public Users findByUsernameAndPassword(Users obj) throws Exception;
	
	public Users findByUsername(String userName) throws Exception;
	
	public Users findBySid(String sid) throws Exception;

}
