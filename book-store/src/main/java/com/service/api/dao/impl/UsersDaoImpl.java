package com.service.api.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.service.api.constant.CommonConstant;
import com.service.api.dao.UsersDao;
import com.service.api.domain.Users;
import com.service.api.exceptions.DatabaseException;
import com.service.api.service.LoggerService;

@Repository
public class UsersDaoImpl implements UsersDao {

	@Autowired
	private LoggerService loggerService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long insert(Users obj) throws Exception {
		
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO USERS (");
        sql.append(" USR_CREATE_DATE ");
        sql.append(",USR_NAME ");
        sql.append(",USR_SURNAME ");
        sql.append(",USR_DATE_OF_BIRTH ");
        sql.append(",USR_USERNAME ");
        sql.append(",USR_PASSWORD ");
        sql.append(",USR_STATUS ");
        sql.append(",USR_CREATOR ");
        sql.append(",USR_CREATOR_ID ");
        sql.append(") VALUES ( ");
        sql.append("sysdate(),");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?");
        sql.append(") ");
        
        KeyHolder keyHolder = new GeneratedKeyHolder();
		int row = jdbcTemplate.update(new PreparedStatementCreator() {
			int i = 1;
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

				ps.setString(i++, obj.getUsrName());
				ps.setString(i++, obj.getUsrSurname());
				
				if(obj.getUsrDateOfBirth()!= null){
					ps.setDate(i++, obj.getUsrDateOfBirth());
				}else{
					ps.setNull(i++, Types.NULL);
				}
				ps.setString(i++, obj.getUsrUsername());
				ps.setString(i++, obj.getUsrPassword());
				ps.setString(i++, obj.getUsrStatus());
				ps.setString(i++, obj.getUsrCreator());
				
				if(obj.getUsrCreatorId()!= null){
					ps.setLong(i++, obj.getUsrCreatorId());
				}else{
					ps.setNull(i++, Types.NULL);
				}

				return ps;
			}
		},keyHolder);

		if(row != 0){
			return (Long) keyHolder.getKey();
		}else{
			return null;
		}
    }

	@Override
	public Users findByUsernameAndPassword(Users obj) throws Exception {
       
		StringBuilder sql = new StringBuilder();
        sql.append("SELECT * ");
        sql.append("FROM USERS ");
        sql.append("WHERE USR_USERNAME = ? AND USR_PASSWORD = ? AND USR_STATUS = 'A'");

        List<Users> objList = null;
        
        try {

        	objList = (List<Users>) jdbcTemplate.query(sql.toString(), new Object[]{obj.getUsrUsername(), obj.getUsrPassword()}, new BeanPropertyRowMapper<Users>(Users.class));
       
        } catch (EmptyResultDataAccessException e) {
            loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
            throw new DatabaseException(e);
        }
        
        return !CollectionUtils.isEmpty(objList)?objList.get(0):null;
    }

	@Override
	public Users findBySid(String sid) throws Exception {
	       
			StringBuilder sql = new StringBuilder();
	        sql.append("SELECT U.* ");
	        sql.append("FROM USERS U ");
	        sql.append("INNER JOIN USERS_SESSION US ON US.USS_USR_ID = USR_ID ");
	        sql.append("WHERE USS_SID = ? AND USS_IS_DELETE <> 'Y'");
	        
	        List<Users> objList = null;
	        
	        try {

	        	objList = (List<Users>) jdbcTemplate.query(sql.toString(), new Object[]{sid}, new BeanPropertyRowMapper<Users>(Users.class));
	       
	        } catch (EmptyResultDataAccessException e) {
	            loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
	            throw new DatabaseException(e);
	        }
	        
	        return !CollectionUtils.isEmpty(objList)?objList.get(0):null;
	    }

	@Override
	public Users findByUsername(String userName) throws Exception {
	       
			StringBuilder sql = new StringBuilder();
	        sql.append("SELECT * ");
	        sql.append("FROM USERS ");
	        sql.append("WHERE USR_USERNAME = ? AND USR_STATUS = 'A'");

	        List<Users> objList = null;
	        
	        try {

	        	objList = (List<Users>) jdbcTemplate.query(sql.toString(), new Object[]{userName}, new BeanPropertyRowMapper<Users>(Users.class));
	       
	        } catch (EmptyResultDataAccessException e) {
	            loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
	            throw new DatabaseException(e);
	        }
	        
	        return !CollectionUtils.isEmpty(objList)?objList.get(0):null;
	    }
	

}
