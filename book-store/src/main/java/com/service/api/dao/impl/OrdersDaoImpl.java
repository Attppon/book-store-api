package com.service.api.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.service.api.constant.CommonConstant;
import com.service.api.dao.OrdersDao;
import com.service.api.domain.Orders;
import com.service.api.exceptions.DatabaseException;
import com.service.api.service.LoggerService;

@Repository
public class OrdersDaoImpl implements OrdersDao {

	@Autowired
	private LoggerService loggerService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Long insert(Orders obj) throws Exception {

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ORDERS (");
		sql.append(" OD_CREATE_DATE ");
		sql.append(",OD_USR_ID ");
		sql.append(",OD_BOOK_ID ");
		sql.append(",OD_BOOK_NAME ");
		sql.append(",OD_BOOK_PRICE ");
		sql.append(",OD_CREATOR ");
		sql.append(",OD_CREATOR_ID ");
		sql.append(") VALUES ( ");
		sql.append("sysdate(),");
		sql.append("?,");
		sql.append("?,");
		sql.append("?,");
		sql.append("?,");
		sql.append("?,");
		sql.append("?");
		sql.append(") ");

		KeyHolder keyHolder = new GeneratedKeyHolder();
		int row = jdbcTemplate.update(new PreparedStatementCreator() {
			int i = 1;
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

				if(obj.getOdUsrId() != null) {
					ps.setLong(i++, obj.getOdUsrId());
				}else{
					ps.setNull(i++, Types.NULL);
				}

				if(obj.getOdBookId() != null) {
					ps.setLong(i++, obj.getOdBookId());
				}else{
					ps.setNull(i++, Types.NULL);
				}

				ps.setString(i++, obj.getOdBookName());

				if (obj.getOdBookPrice() != null) {
					ps.setDouble(i++, obj.getOdBookPrice());
				} else {
					ps.setNull(i++, Types.NULL);
				}

				ps.setString(i++, obj.getOdCreator());

				if (obj.getOdCreatorId() != null) {
					ps.setLong(i++, obj.getOdCreatorId());
				} else {
					ps.setNull(i++, Types.NULL);
				}

				return ps;
			}
		},keyHolder);

		if(row != 0){
			return (Long) keyHolder.getKey();
		}else{
			return null;
		}
	}

	@Override
	public List<Orders> findByUserId(Long usrId) throws Exception {

		List<Orders> objList = null;
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * ");
		sql.append("FROM ORDERS WHERE OD_USR_ID = ? ");

		try {

			objList = (List<Orders>) jdbcTemplate.query(sql.toString(), new Object[]{usrId}, new BeanPropertyRowMapper<Orders>(Orders.class));

		} catch (DataAccessException e) {
			loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_ACCESS_SQL_EXCEPTION, e);
			throw new DatabaseException(e);

		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
			throw new DatabaseException(e);
		}

		return objList;
	}

	@Override
	public int deleteByUserId(Long userId) throws Exception {
		
		StringBuilder sql = new StringBuilder();
        int row = 0;
        
        try {
        	
            sql.append(" DELETE FROM  ORDERS ");
			sql.append(" WHERE OD_USR_ID = ? ");
			
            row = jdbcTemplate.update(sql.toString(), new Object[]{userId});
	
		} catch (DataAccessException e) {
			loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_ACCESS_SQL_EXCEPTION, e);
			throw new DatabaseException(e);

		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
			throw new DatabaseException(e);
		}
		
        return row;
	}

}
