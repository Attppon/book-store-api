package com.service.api.vo;

public class Books {

	private Long id;
	private String name;
	private String author;
	private Double price;
	private boolean is_recommended;
	
	public Books(){
		super();
	}
	
	public Books(Long id, String name, String author, Double price, boolean is_recommended) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.price = price;
		this.is_recommended = is_recommended;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public boolean isIs_recommended() {
		return is_recommended;
	}
	public void setIs_recommended(boolean is_recommended) {
		this.is_recommended = is_recommended;
	}
	
	@Override
	public String toString() {
		return "Books [id=" + id + ", name=" + name + ", author=" + author + ", price=" + price
				+ ", is_recommended=" + is_recommended + "]";
	}
	
}
