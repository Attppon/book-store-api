package com.service.api.exceptions;

public class ExternalExceptionConnection extends Exception {

    public ExternalExceptionConnection() {

    }

    public ExternalExceptionConnection(String messageCode) {
        super(messageCode);
    }

    public ExternalExceptionConnection(String messageCode, Throwable cause) {
        super(messageCode, cause);
    }

    public ExternalExceptionConnection(Throwable cause) {
        super(cause);
    }

    public ExternalExceptionConnection(String messageCode, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(messageCode, cause, enableSuppression, writableStackTrace);
    }

}
