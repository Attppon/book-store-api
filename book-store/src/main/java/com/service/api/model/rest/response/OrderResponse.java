package com.service.api.model.rest.response;

import com.service.api.model.rest.BaseResponse;

public class OrderResponse extends BaseResponse {

    private Double price;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "OrderResponse [price=" + price + "]";
	}
    
	
}
