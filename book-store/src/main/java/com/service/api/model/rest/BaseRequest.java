package com.service.api.model.rest;

public class BaseRequest {

    private String sid;
    private String language;
    private String decryptData;
    private boolean encryptFlag;
    private String ssoId;
    private String ipAddress;
    private String requestURI;
    
    public BaseRequest() {
		super();
	}
    
    public BaseRequest(String sid) {
		this.sid = sid;
	}

	public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDecryptData() {
        return decryptData;
    }

    public void setDecryptData(String decryptData) {
        this.decryptData = decryptData;
    }

    public boolean isEncryptFlag() {
        return encryptFlag;
    }

    public void setEncryptFlag(boolean encryptFlag) {
        this.encryptFlag = encryptFlag;
    }

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	@Override
	public String toString() {
		return "BaseRequest [sid=" + sid + ", language=" + language + ", decryptData=" + decryptData + ", encryptFlag="
				+ encryptFlag + ", ssoId=" + ssoId + ", ipAddress=" + ipAddress + ", requestURI=" + requestURI + "]";
	}

}
