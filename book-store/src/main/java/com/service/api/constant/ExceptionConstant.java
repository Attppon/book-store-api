package com.service.api.constant;

public class ExceptionConstant {

    private ExceptionConstant() {
        throw new IllegalStateException();
    }

    //<---- GLOBAL_EXCEPTION ---->  
    public static final String SYSTEM_DISABLE = "000500";
    public static final String SERVICE_EXCEPTION = "กรุณาติดต่อผู้ดูแลระบบ";
    public static final String DATABASE_EXCEPTION = "กรุณาติดต่อผู้ดูแลระบบ";
    public static final String EXTERNAL_EXCEPTION = "กรุณาติดต่อผู้ดูแลระบบ";
    public static final String EXTERNAL_CONNECTION_EXCEPTION = "090000";
    public static final String INTERNAL_CONNECTION_EXCEPTION = "000800";

    public static final String ERROR_CODE_SID_INVALID = "010001";
    public static final String ERROR_CODE_LANGUAGE_INVALID = "010002";
    public static final String ERROR_CODE_SESSION_EXPIRED = "010003";
    public static final String ERROR_CODE_DATA_NOT_FOUND = "010004";
    public static final String ERROR_CODE_NOT_ALLOW_TO_ACCESS = "100005";



}
