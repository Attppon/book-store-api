package com.service.api.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.service.api.constant.CommonConstant;
import com.service.api.service.LoggerService;

@Service
public class LoggerServiceImpl implements LoggerService {

	private static final String START_SQUARE_BRACKETS = "[";
	private static final String END_SQUARE_BRACKETS = "]";
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	private final Logger accessLogger = LoggerFactory.getLogger(CommonConstant.ACCESS_LOG);
	private final Logger systemLogger = LoggerFactory.getLogger(CommonConstant.SYSTEM_LOG);
	private final Logger errorLogger = LoggerFactory.getLogger(CommonConstant.ERROR_LOG);

	@Override
	public void accessLogger(Date startDate, Date endDate, String refID, String result, Object input, Object output,
			String level) {
		StringBuilder sb = new StringBuilder();

		// ReferenceID
		sb.append("RefID=");
		sb.append(START_SQUARE_BRACKETS);
		sb.append(refID == null ? "" : refID);
		sb.append(", ");

		// Time
		sb.append(dateFormat.format(startDate));
		sb.append(", ");
		sb.append(dateFormat.format(endDate));
		sb.append(", ");
		sb.append((endDate.getTime() - startDate.getTime()));
		sb.append(END_SQUARE_BRACKETS);
		sb.append(" ");

		// Result
		sb.append("Result=");
		sb.append(START_SQUARE_BRACKETS);
		sb.append(result == null ? "" : result);
		sb.append(END_SQUARE_BRACKETS);
		sb.append(" ");

		// Resquest
		sb.append("Request=");
		sb.append(START_SQUARE_BRACKETS);
		sb.append(input == null ? "" : input.toString());
		sb.append(END_SQUARE_BRACKETS);
		sb.append(" ");

		// Response
		sb.append("Response=");
		sb.append(START_SQUARE_BRACKETS);
		sb.append(output == null ? "" : output.toString());
		sb.append(END_SQUARE_BRACKETS);

		String log = sb.toString();

		if (CommonConstant.LOG_LEVEL_DEBUG.equalsIgnoreCase(level)) {
			accessLogger.debug(log);
		} else if (CommonConstant.LOG_LEVEL_INFO.equalsIgnoreCase(level)) {
			accessLogger.info(log);
		} else if (CommonConstant.LOG_LEVEL_WARN.equalsIgnoreCase(level)) {
			accessLogger.warn(log);
		} else if (CommonConstant.LOG_LEVEL_ERROR.equalsIgnoreCase(level)) {
			accessLogger.error(log);
		} else {
			accessLogger.trace(log);
		}
	}

	@Override
	public void systemLogger(String refID, String message, Object result, String level) {
		StringBuilder sb = new StringBuilder();

		// ReferenceID
		sb.append("RefID=[");
		sb.append(refID == null ? "" : refID);
		sb.append("]");
		sb.append(" ");

		//Result
		sb.append("Message=[");
		sb.append(message == null ? "" : message);
		sb.append("]");
		sb.append(" ");

		//Object
		sb.append("Result=[");
		sb.append(result == null ? "" : result.toString());
		sb.append("]");
		sb.append(" ");

		String log = sb.toString();

		if (CommonConstant.LOG_LEVEL_DEBUG.equalsIgnoreCase(level)) {
			systemLogger.debug(log);
		} else if (CommonConstant.LOG_LEVEL_INFO.equalsIgnoreCase(level)) {
			systemLogger.info(log);
		} else if (CommonConstant.LOG_LEVEL_WARN.equalsIgnoreCase(level)) {
			systemLogger.warn(log);
		} else if (CommonConstant.LOG_LEVEL_ERROR.equalsIgnoreCase(level)) {
			systemLogger.error(log);
		} else {
			systemLogger.trace(log);
		}
	}

	@Override
	public void systemLogger(String ipClient, String refID, String message, Object result, String level) {
		StringBuilder sb = new StringBuilder();

		// IP Client
		sb.append("IP=[");
		sb.append(ipClient == null ? "" : ipClient);
		sb.append("]");
		sb.append(" ");

		// ReferenceID
		sb.append("RefID=[");
		sb.append(refID == null ? "" : refID);
		sb.append("]");
		sb.append(" ");

		// Result
		sb.append("Message=[");
		sb.append(message == null ? "" : message);
		sb.append("]");
		sb.append(" ");

		// Object
		sb.append("Result=[");
		sb.append(result == null ? "" : result.toString());
		sb.append("]");
		sb.append(" ");

		String log = sb.toString();

		if (CommonConstant.LOG_LEVEL_DEBUG.equalsIgnoreCase(level)) {
			systemLogger.debug(log);
		} else if (CommonConstant.LOG_LEVEL_INFO.equalsIgnoreCase(level)) {
			systemLogger.info(log);
		} else if (CommonConstant.LOG_LEVEL_WARN.equalsIgnoreCase(level)) {
			systemLogger.warn(log);
		} else if (CommonConstant.LOG_LEVEL_ERROR.equalsIgnoreCase(level)) {
			systemLogger.error(log);
		} else {
			systemLogger.trace(log);
		}
	}

	@Override
	public void printStackTraceToErrorLog(String refID, String exceptionClassName, Throwable e) {
		StringBuilder str = new StringBuilder();

		str.append("[RefID: ").append(refID).append("] ");
		str.append("[Type: ").append(exceptionClassName).append("] ");
		str.append("[MessageCode: ").append(e.getMessage()).append("] ");

		StackTraceElement elements[] = e.getStackTrace();
		if (elements != null && elements.length > 0) {
			str.append(e.toString());
			for (int i = 0, n = elements.length; i < n; i++) {
				str.append(" at ");
				str.append(elements[i].getClassName()).append(" (").append(elements[i].getMethodName())
				.append(":").append(elements[i].getLineNumber()).append(")\n");
			}

		} else {
			str.append(e.toString());
		}

		if (e.getCause() != null) {
			StackTraceElement elements2[] = e.getCause().getStackTrace();
			if (elements2 != null && elements2.length != 0) {
				str.append(" Caused by : ");
				str.append(e.getCause().toString());
				str.append(" at ");
				for (StackTraceElement elements21 : elements2) {
					str
					.append(" at ")
					.append(elements21.getClassName())
					.append("(").append(elements21.getMethodName())
					.append(":").append(elements21.getLineNumber()).append(")\n");
				}

			} else {
				str.append(e.getCause().toString());
			}
		}
		String log = str.toString();
		errorLogger.error(log);
	}
	
	@Override
    public void printStackTraceToErrorLog(String ipClient, String refID, String exceptionClassName, Throwable e) {
        StringBuilder str = new StringBuilder();

        str.append("[IP: ").append(ipClient).append("] ");
        str.append("[RefID: ").append(refID).append("] ");
        str.append("[Type: ").append(exceptionClassName).append("] ");
        str.append("[MessageCode: ").append(e.getMessage()).append("] ");

        StackTraceElement elements[] = e.getStackTrace();
        if (elements != null && elements.length > 0) {
            str.append(e.toString());
            for (int i = 0, n = elements.length; i < n; i++) {
                str.append(" at ");
                str.append(elements[i].getClassName()).append(" (").append(elements[i].getMethodName())
                        .append(":").append(elements[i].getLineNumber()).append(")\n");
            }

        } else {
            str.append(e.toString());
        }

        if (e.getCause() != null) {
            StackTraceElement elements2[] = e.getCause().getStackTrace();
            if (elements2 != null && elements2.length != 0) {
                str.append(" Caused by : ");
                str.append(e.getCause().toString());
                str.append(" at ");
                for (StackTraceElement elements21 : elements2) {
                    str
                            .append(" at ")
                            .append(elements21.getClassName())
                            .append("(").append(elements21.getMethodName())
                            .append(":").append(elements21.getLineNumber()).append(")\n");
                }

            } else {
                str.append(e.getCause().toString());
            }
        }
        String log = str.toString();
        errorLogger.error(log);
    }
}
