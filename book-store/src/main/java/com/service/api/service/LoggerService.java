package com.service.api.service;

import java.util.Date;

public interface LoggerService {

	public void accessLogger(Date startDate, Date endDate, String refID, String result, Object input,  Object output, String level) throws Exception;

	public void systemLogger(String refID, String message, Object result, String level);

	public void systemLogger(String ipClient, String refID, String message, Object result, String level);

	public void printStackTraceToErrorLog(String refID, String exceptionClassName, Throwable e);

	public void printStackTraceToErrorLog(String ipClient, String refID, String exceptionClassName, Throwable e);
}
