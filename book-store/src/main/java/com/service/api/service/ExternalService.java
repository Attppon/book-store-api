package com.service.api.service;

import java.util.List;

import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.response.BooksResponse;
import com.service.api.vo.Books;

public interface ExternalService {

	 public BooksResponse getBooks(BaseRequest request) throws Exception;
	 public List<Books> getBookAll() throws Exception;
	 public List<Books> getBookByInBookId(List<Long> id) throws Exception;
	 
}
