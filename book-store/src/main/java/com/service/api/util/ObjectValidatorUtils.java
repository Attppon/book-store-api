package com.service.api.util;

import static com.service.api.util.StringUtils.isNotEmptyOrNull;
import java.math.BigInteger;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class ObjectValidatorUtils {

    public final String APPLICATION_VALUE_TYPE_INTEGER = "Integer";
    public final String APPLICATION_VALUE_TYPE_FLOAT = "Float";
    public final String APPLICATION_VALUE_TYPE_DOUBLE = "Double";
    public final String APPLICATION_VALUE_TYPE_LONG = "Long";
    public final String APPLICATION_VALUE_TYPE_BIGINTEGER = "BigInteger";
    public final String APPLICATION_VALUE_TYPE_BOOLEAN = "Boolean";
    public final String APPLICATION_VALUE_PATTERN_MOBILE = "^0([0-9])\\d{8}$";
    
    public boolean validateMandatory(String str) {
        return !(null == str || "".equals(str.trim()) || str.trim().length() == 0 || "null".equalsIgnoreCase(str));
    }

    public static boolean validatePattern(String input, String regex) {
        if (isNotEmptyOrNull(regex)) {
            Pattern pattern = Pattern.compile(regex);
            if (!pattern.matcher(input).matches()) {
                return false;
            }
        }
        return true;
    }

    public boolean validateLength(String input, int maxLength) {
        return input != null && (input.length() <= maxLength);
    }

    public boolean validateLengthEqual(String input, int maxLength) {
        return input != null && (input.length() == maxLength);
    }

    public boolean validateType(String input, String validateType) {

        try {
            if (isNotEmptyOrNull(input)) {
                if (APPLICATION_VALUE_TYPE_INTEGER.equalsIgnoreCase(validateType)) {
                    Integer.parseInt(input);

                }
                if (APPLICATION_VALUE_TYPE_FLOAT.equalsIgnoreCase(validateType)) {
                    Float.parseFloat(input);

                }
                if (APPLICATION_VALUE_TYPE_DOUBLE.equalsIgnoreCase(validateType)) {
                    Double.parseDouble(input);

                }
                if (APPLICATION_VALUE_TYPE_LONG.equalsIgnoreCase(validateType)) {
                    Long.parseLong(input);

                }
                if (APPLICATION_VALUE_TYPE_BIGINTEGER.equalsIgnoreCase(validateType)) {
                    BigInteger.valueOf(Long.parseLong(input));

                }
                if (APPLICATION_VALUE_TYPE_BOOLEAN.equalsIgnoreCase(validateType)) {
                    return "true".equalsIgnoreCase(input) || "false".equalsIgnoreCase(input);

                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
