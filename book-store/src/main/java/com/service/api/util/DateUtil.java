package com.service.api.util;

import com.service.api.constant.CommonConstant;
import com.service.api.exceptions.ServiceException;
import com.service.api.service.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DateUtil {

    private DateUtil() {
        throw new IllegalStateException("Utility class");
    }

    @Autowired
    private static LoggerService loggerService;

    public static final Locale DEFAULT_LOCALE = new Locale("en", "EN");
    public static final Locale THAI_LOCALE = new Locale("th", "TH");
    public static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_DATETIME_FORMAT2 = "dd/MM/yyyy HH:mm:ss";
    public static final String DEFAULT_DATETIME_FORMAT3 = "yyyy-MM-dd HH:mm:ss";
    public static final String LI2_DATETIME_FORMAT = "yyyyMMddHHmmss";
    public static final String DEFAULT_DATETIME_FORMAT = "dd/MM/yyyy HH:mm";
    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
    public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String DEFAULT_DATETIME_YYYY_DASH_MM_DASH_DD = "yyyy-MM-dd";
    public static final String DEFAULT_DATETIME_DD_DASH_MM_DASH_YYYY = "dd-MM-yyyy";
    public static final String DEFAULT_DATETIME_YYYY_DASH_MM_DASH_DD_00_00_00 = "yyyy-MM-dd 00:00:00";
    public static final String DEFAULT_DATETIME_YYYY_DASH_MM_DASH_DD_23_59_59 = "yyyy-MM-dd 23:59:59";
    public static final String DEFAULT_DATETIME_FORMAT7 = "yyyyMMddHHmmssSSS";
    
    public static final Locale APPLICATION_LOCALE_TH = new Locale("th", "TH");
    public static final String TIME_HHMM_FORMAT4 = "HH:mm";

    public static final String DEFAULT_TIME_FORMAT_WS = "yyyyMMdd'T'HHmmss.SSS 'GMT'XXX";
    
    public static final Locale APPLICATION_LOCALE = Locale.US;

    public static Date parseStringtoDate(String dateString, String pattern, String language) throws ServiceException, ParseException {
        SimpleDateFormat format;

        if (StringUtils.isNotEmptyOrNull(dateString) && StringUtils.isNotEmptyOrNull(pattern)) {
            if (CommonConstant.LANGUAGE_TH.equalsIgnoreCase(language)) {
                format = new SimpleDateFormat(pattern, THAI_LOCALE);
            } else {
                format = new SimpleDateFormat(pattern, DEFAULT_LOCALE);
            }

            return format.parse(dateString);

        } else {
            return null;
        }
    }

    public static String formatDatetoString(Date date, String pattern, String language) {
        SimpleDateFormat format;

        if (date != null && StringUtils.isNotEmptyOrNull(pattern)) {
            if (CommonConstant.LANGUAGE_TH.equalsIgnoreCase(language)) {
                format = new SimpleDateFormat(pattern, THAI_LOCALE);
            } else {
                format = new SimpleDateFormat(pattern, DEFAULT_LOCALE);
            }

            return format.format(date);

        } else {
            return null;
        }

    }

    public static boolean validateDate(String dateString, String pattern, String language){
        SimpleDateFormat format;
        boolean isDateValid = false;

        try {
            if (StringUtils.isNotEmptyOrNull(dateString) && StringUtils.isNotEmptyOrNull(pattern)) {
                if (CommonConstant.LANGUAGE_TH.equalsIgnoreCase(language)) {
                    format = new SimpleDateFormat(pattern, THAI_LOCALE);
                } else {
                    format = new SimpleDateFormat(pattern, DEFAULT_LOCALE);
                }

                format.parse(dateString);
                isDateValid = true;
            }

        } catch (ParseException e) {
            isDateValid = false;
        }

        return isDateValid;
    }

    public static String changeFormat(String dateOld) {
        String newDate = "";
        if (dateOld != null && !"".equals(dateOld) && dateOld.indexOf("-") != -1) {
            String arr[] = dateOld.split("-");
            newDate = arr[2] + "/" + arr[1] + "/" + arr[0];
        }

        return newDate;
    }

    public static void main(String[] args) throws ServiceException, ParseException {
        String dateStr = "18/02/2020 23:59:59";
        Date date = DateUtil.parseStringtoDate(dateStr, "dd/MM/yyyy HH:mm:ss", "en");

        StringBuilder sb = new StringBuilder();
        sb
                .append("DateStr : ")
                .append(dateStr)
                .append(System.lineSeparator())
                .append("Date : ")
                .append(date);
    }

    public static String trimLowerDate(String d) {
        if (d != null) {
            return d + " 00:00:00";
        }
        return null;
    }

    public static Date trimDate(Date d) {
        if (d != null) {
            Calendar cal = Calendar.getInstance(Locale.US);
            cal.setTime(d);

            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            return cal.getTime();
        }
        return null;
    }

    public static String getDateStr(Date date, boolean isTH) {
        if (date == null) {
            return "";
        } else {
            Calendar cal = Calendar.getInstance(Locale.US);
            cal.setTime(date);
            if (isTH) {
                cal.add(Calendar.YEAR, 543);
            }
            SimpleDateFormat dFormat = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT3);
            return dFormat.format(cal.getTime());
        }
    }

    public static String trimUpperDate(String d) {
        if (d != null) {
            return d + " 23:59:59";
        }
        return null;
    }

    public static String getCurrentDateString(String pattern) {
        return format(getCurrentDateTime(), pattern);
    }

    public static Date getCurrentDateTime() {
        return Calendar.getInstance().getTime();
    }

    public static String format(java.util.Date date, String pattern) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat sdf = getSimpleDateFormat(pattern, APPLICATION_LOCALE);
        return sdf.format(date);
    }

    public static SimpleDateFormat getSimpleDateFormat(String pattern, Locale locale) {
        return new SimpleDateFormat(pattern, locale);
    }

    public static String converStringToDate(String date) {
        String dateStr = "";
        if (date != null && !"".equals(date) && !"99999999".equals(date)) {
            date = date.replace("-", "");
            String y = "";
            String m = "";
            String d = "";

            y = date.substring(0, 4);
            m = date.substring(4, 6);
            d = date.substring(6, 8);
            dateStr = d + "/" + m + "/" + y;
        }
        return dateStr;

    }

    public static String getDateFormatThai(java.util.Date rcvDate, String format) {
        String dateStr = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format, THAI_LOCALE);
            dateStr = formatter.format(rcvDate);
        } catch (Exception e) {
            System.err.println("Exception :: getDateFormat" + e.getMessage());
            return null;
        }
        return dateStr;
    }

    public static Date getCurDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public static String dateToStringTH(Date date, String format) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(format, new Locale("th", "TH")).format(date);
    }

    public static Date addDate(Date date, int dateAdd) {
        Calendar cal;
        if (date == null) {
            return null;
        } else {
            cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, dateAdd);
            return cal.getTime();
        }
    }
    
    public static java.util.Date parseEng(String dateString, String pattern)
            throws java.text.ParseException {
        java.util.Date dateThai = null;
		
        try {
			 if(dateString!=null&&!"".equals(dateString)){
		            SimpleDateFormat sdf = getSimpleDateFormat(pattern, APPLICATION_LOCALE);
		            dateThai = sdf.parse(dateString);
		        }
		        return dateThai;
		} catch (Exception e) {
			 return null;
		}
       
    }
    
    public static String getCurrentDateString() {
        return getCurrentDateString(DEFAULT_DATE_FORMAT);
    }

    public static long getDifferenceDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
    
 	
 	public static String getCurrentTimeFormat2(String pattern) throws Exception {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, APPLICATION_LOCALE_TH);

        return sdf.format(date);
    }

    public static java.util.Date parseThai(String dateString, String pattern)
            throws java.text.ParseException {
        java.util.Date dateThai = null;

        if (dateString != null && !"".equals(dateString)) {
            SimpleDateFormat sdf = getSimpleDateFormat(pattern, APPLICATION_LOCALE_TH);
            dateThai = sdf.parse(dateString);
        }
        return dateThai;
    }
    
    public static java.sql.Timestamp toTimeStamp(java.util.Date date) {
        return date == null ? null : new java.sql.Timestamp(date.getTime());
    }
    
    public static java.sql.Date toSqlDate(java.util.Date date) {
        return date == null ? null : new java.sql.Date(date.getTime());
    }
    
    public static java.sql.Date parseThai2SqlDate(String dateString, String pattern)
            throws java.text.ParseException {
        return toSqlDate(parseThai(dateString, pattern));
    }

    public static String getDateFormat(java.util.Date rcvDate, String format) {
        String dateStr = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format, APPLICATION_LOCALE);
            dateStr = formatter.format(rcvDate);
        } catch (Exception e) {
            System.err.println("Exception :: getDateFormat" + e.getMessage());
            return null;
        }
        return dateStr;
    }

    public static String getTimeFormat(java.sql.Timestamp date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = getSimpleDateFormat(DEFAULT_TIME_FORMAT, APPLICATION_LOCALE);
        return sdf.format(date);
    }
    
    public boolean checkValidateDate(String dateStr) {
    	boolean valid = true;
    	try {

    		Date dateConvert = null;

    		if (StringUtils.isNotEmptyOrNull(dateStr)) {
    			java.util.Date tmpDate = DateUtil.parseEng(dateStr, DateUtil.DEFAULT_DATETIME_YYYY_DASH_MM_DASH_DD);
    			dateConvert = tmpDate;
    		}

    		if (dateConvert == null) {
    			valid = false;
    		}
    	} catch (ParseException e) {
    		valid = false;
    	}
    	return valid;
    }
    
}
