package com.service.api.util;

import com.google.gson.Gson;

public class JSONUtil {

    private JSONUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String transformObjectToString(Object obj) {
        if (obj != null) {
            Gson gsonObject = new Gson();
            return gsonObject.toJson(obj);

        }

        return null;
    }

    public static Object transformStringToObject(String jsonValue, Class<?> clazz) {
        if (StringUtils.isNotEmptyOrNull(jsonValue)) {
            Gson gsonObject = new Gson();
            return gsonObject.fromJson(jsonValue, clazz);

        }

        return null;
    }
}
