package com.service.api.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.service.api.exceptions.ServiceValidation;
import com.service.api.util.ObjectValidatorUtils;

public class AuthenticationControllerValidator {

    @Autowired
    private ObjectValidatorUtils validator;

    protected void loginValidation(String username, String password) throws ServiceValidation {
    	
        if (!validator.validateMandatory(username)) {
            throw new ServiceValidation("username is required");

        }

        if (!validator.validateMandatory(password)) {
            throw new ServiceValidation("password is required");

        }
        
    }

}
